#ifndef SETTINGS_H
#define SETTINGS_H

#include <QMap>

class QSettings;
class QVariant;

namespace CloseProjectFiles
{
	namespace Internal
	{
		class Settings
		{
			public:
				enum class SettingName {AllowClose};

				Settings(QSettings *storage);

				void save();
				void load();

				QVariant value(SettingName key) const;
				void setValue(SettingName key, const QVariant &value);

			private:
				QSettings *m_storage = nullptr;
				QMap<SettingName, QVariant> m_settings;
		};
	}
}

#endif // SETTINGS_H
