# CloseProjectFiles #

A plugin for Qt Creator, implements the possibility of closing the project files opened in the editor, when you close the project through the menu - Close Project "project name". 
The plugin has been tested with qt creator 2.8.1 and library qt-4.8.6.
Source code of the plugin uses the syntax of C++11.

### Setup before building ###

Prior to build a plug-in, in project file "closeprojectfiles.pro" you must specify the path to the Qt Creator source, and the path to the folder where it is installed. To do this, you must change the value of variables: QTCREATOR_SOURCES and IDE_BUILD_TREE.

### Usage ###

If the build succeeds, then in the window of Installed Plugins, under Utilites you will see the plugin under the name "CloseProjectFiles". After turning it on and restart the IDE, will appears menu "Tools - ProjectFiles" with the item "Close Project Files Together With the Project". Enabling or disabling this menu item, you can enable or disable the functionality of the plugin.
# 
#
#
**Note**:  this plugin will not be build in Qt Creator 3 and later, because there has changed API.