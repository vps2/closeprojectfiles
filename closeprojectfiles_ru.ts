<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>CloseProjectFiles::Internal::CloseProjectFilesPlugin</name>
    <message>
        <location filename="closeprojectfilesconstants.h" line="21"/>
        <source>Project Files</source>
        <translation>Файлы проекта</translation>
    </message>
    <message>
        <location filename="closeprojectfilesconstants.h" line="23"/>
        <source>Close Project Files Together With Project</source>
        <translation>Закрывать при закрытии проекта</translation>
    </message>
</context>
</TS>
