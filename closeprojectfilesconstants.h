#ifndef CLOSEPROJECTFILESCONSTANTS_H
#define CLOSEPROJECTFILESCONSTANTS_H

namespace CloseProjectFiles
{
	namespace Constants
	{
		
		const char ACTION_ID[] = "CloseProjectFiles.Action";
		const char MENU_ID[] = "CloseProjectFiles.Menu";

		//Settings
		const char SETTINGS_GROUP[] = "ProjectExplorer";
		const char SETTING_NAME[] = "Settings/AutoCloseProjectFiles";

		//Translate files name
		const char TRANSLATE_FILES_PATH[] = ":/lang";
		const char TRANSLATE_FILES_PREFIX_NAME[] = "closeprojectfiles_";

		//Other
		const char MENU_TITLE[] = QT_TRANSLATE_NOOP("CloseProjectFiles::Internal::CloseProjectFilesPlugin",
																  "Project Files");
		const char ACTION_TEXT[] = QT_TRANSLATE_NOOP("CloseProjectFiles::Internal::CloseProjectFilesPlugin",
																	"Close Project Files Together With Project");
		
	} // namespace CloseProjectFiles
} // namespace Constants

#endif // CLOSEPROJECTFILESCONSTANTS_H

