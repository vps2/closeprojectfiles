#include "closeprojectfilesplugin.h"
#include "closeprojectfilesconstants.h"
#include "closefilesmanager.h"
#include "settings.h"

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

#include <QAction>
#include <QMenu>
#include <QtPlugin>
#include <QTranslator>

using namespace CloseProjectFiles::Internal;

CloseProjectFilesPlugin::CloseProjectFilesPlugin()
{
	// Create your members
}

CloseProjectFilesPlugin::~CloseProjectFilesPlugin()
{
	// Unregister objects from the plugin manager's object pool
	// Delete members
}

bool CloseProjectFilesPlugin::initialize(const QStringList &arguments, QString *errorString)
{
	// Register objects in the plugin manager's object pool
	// Load settings
	// Add actions to menus
	// Connect to other plugins' signals
	// In the initialize method, a plugin can be sure that the plugins it
	// depends on have initialized their members.
	
	Q_UNUSED(arguments)

	try
	{
		createClassMembers();

		installTranslator();

		m_settings->load();

		QAction *menuAction = createAction();
		createMenuForAction(menuAction);
	}
	catch(const std::exception &ex)
	{
		*errorString = QLatin1String(ex.what());
		return false;
	}

	bool allowClose = m_settings->value(Settings::SettingName::AllowClose).toBool();
	changeSettings(allowClose);

	return true;
}

void CloseProjectFilesPlugin::extensionsInitialized()
{
	// Retrieve objects from the plugin manager's object pool
	// In the extensionsInitialized method, a plugin can be sure that all
	// plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag CloseProjectFilesPlugin::aboutToShutdown()
{
	// Save settings
	// Disconnect from signals that are not needed during shutdown
	// Hide UI (if you add UI that is not in the main window directly)
	return SynchronousShutdown;
}

QAction* CloseProjectFilesPlugin::createAction()
{
	QAction *menuAction = new QAction(tr(Constants::ACTION_TEXT), this);
	menuAction->setCheckable(true);
	bool allowClose = m_settings->value(Settings::SettingName::AllowClose).toBool();
	menuAction->setChecked(allowClose);

	connect(menuAction, SIGNAL(triggered(bool)), this, SLOT(changeSettings(bool)));

	return menuAction;
}

void CloseProjectFilesPlugin::createMenuForAction(QAction *action)
{
	Core::ActionManager *actionManager = Core::ICore::actionManager();
	Core::Context globalContext(Core::Constants::C_GLOBAL);
	Core::Command *menuCommad = actionManager->registerAction(action,
																				 Constants::ACTION_ID,
																				 globalContext);

	Core::ActionContainer *menu = actionManager->createMenu(Constants::MENU_ID);
	menu->menu()->setTitle(tr(Constants::MENU_TITLE));
	menu->addAction(menuCommad);
	actionManager->actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);
}

void CloseProjectFilesPlugin::changeSettings(bool checked)
{
	m_closeFilesManager->allowCloseFiles(checked);

	bool oldStatus = m_settings->value(Settings::SettingName::AllowClose).toBool();
	bool currStatus = checked;

	if(oldStatus != currStatus)
	{
		m_settings->setValue(Settings::SettingName::AllowClose, currStatus);
		m_settings->save();
	}
}

void CloseProjectFilesPlugin::createClassMembers()
{
	m_settings.reset(new Settings(Core::ICore::settings()));
	m_closeFilesManager.reset(new CloseFilesManager());
	m_translator.reset(new QTranslator());
}

void CloseProjectFilesPlugin::installTranslator()
{
	const QString locale = Core::ICore::userInterfaceLanguage();

	if (!locale.isEmpty())
	{
		const QString translationFileName = QLatin1String(Constants::TRANSLATE_FILES_PREFIX_NAME) + locale;
		const QString translationFilePath = QLatin1String(Constants::TRANSLATE_FILES_PATH);
		if(m_translator->load(translationFileName, translationFilePath))
		{
			qApp->installTranslator(m_translator.data());
		}
	}
}

Q_EXPORT_PLUGIN2(CloseProjectFiles, CloseProjectFilesPlugin)

