#ifndef CLOSEFILESMANAGER_H
#define CLOSEFILESMANAGER_H

#include <QObject>

namespace ProjectExplorer
{
	class ProjectExplorerPlugin;
	class Project;
}

namespace CloseProjectFiles
{
	namespace Internal
	{
		class CloseFilesManager : public QObject
		{
				Q_OBJECT

			public:
				explicit CloseFilesManager(QObject *parent = nullptr);

				void allowCloseFiles(bool allow);

			private slots:
				bool closeProjectFiles(ProjectExplorer::Project *project);

			private:
				bool m_allowClose;

				ProjectExplorer::ProjectExplorerPlugin *m_projectExplorerPlugin = nullptr;
		};
	}
}

#endif // CLOSEFILESMANAGER_H
