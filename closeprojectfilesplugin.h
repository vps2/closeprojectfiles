#ifndef CLOSEPROJECTFILES_H
#define CLOSEPROJECTFILES_H

#include "closeprojectfiles_global.h"

#include <extensionsystem/iplugin.h>

class QTranslator;
class QAction;

namespace CloseProjectFiles
{
	namespace Internal
	{
		class Settings;
		class CloseFilesManager;

		//------------------------------------------------------------------------
		
		class CloseProjectFilesPlugin : public ExtensionSystem::IPlugin
		{
				Q_OBJECT

			public:
				CloseProjectFilesPlugin();
				~CloseProjectFilesPlugin();
				
				bool initialize(const QStringList &arguments, QString *errorString) override;
				void extensionsInitialized() override;
				ShutdownFlag aboutToShutdown() override;

			private slots:
				void changeSettings(bool checked);

			private:
				void createClassMembers();
				void installTranslator();
				QAction* createAction();
				void createMenuForAction(QAction *action);

				QScopedPointer<Settings> m_settings;
				QScopedPointer<CloseFilesManager> m_closeFilesManager;
				QScopedPointer<QTranslator> m_translator;
		};

		//------------------------------------------------------------------------
		
	} // namespace Internal
} // namespace CloseProjectFiles

#endif // CLOSEPROJECTFILES_H

