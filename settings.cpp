#include "settings.h"
#include "closeprojectfilesconstants.h"

#include <QSettings>
#include <QVariant>

using namespace CloseProjectFiles::Internal;

Settings::Settings(QSettings *storage) :
	m_storage(storage)
{
	Q_ASSERT(m_storage != nullptr);
}

void Settings::save()
{
	m_storage->beginGroup(QLatin1String(Constants::SETTINGS_GROUP));

	m_storage->setValue(QLatin1String(Constants::SETTING_NAME),
							 value(SettingName::AllowClose));

	m_storage->endGroup();

	m_storage->sync();
}

void Settings::load()
{
	m_storage->beginGroup(QLatin1String(Constants::SETTINGS_GROUP));

	setValue(SettingName::AllowClose,
				m_storage->value(QLatin1String(Constants::SETTING_NAME), false));

	m_storage->endGroup();
}

QVariant Settings::value(SettingName key) const
{
	return m_settings.value(key);
}

void Settings::setValue(SettingName key, const QVariant &value)
{
	m_settings[key] = value;
}
