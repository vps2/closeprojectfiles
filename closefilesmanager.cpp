#include "closefilesmanager.h"

#include <coreplugin/icore.h>

#include <extensionsystem/pluginmanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/session.h>
#include <projectexplorer/project.h>

#include <coreplugin/editormanager/ieditor.h>
#include <coreplugin/editormanager/editormanager.h>

using namespace CloseProjectFiles::Internal;

CloseFilesManager::CloseFilesManager(QObject *parent) :
	QObject(parent),
	m_allowClose(false)
{
	ExtensionSystem::PluginManager
			*pluginManager = ExtensionSystem::PluginManager::instance();

	m_projectExplorerPlugin = pluginManager->getObject<ProjectExplorer::ProjectExplorerPlugin>();
}

void CloseFilesManager::allowCloseFiles(bool allow)
{
	if(m_allowClose == allow)
	{
		return;
	}

	m_allowClose = allow;

	m_allowClose ? connect(m_projectExplorerPlugin->session(),
								  SIGNAL(aboutToRemoveProject(ProjectExplorer::Project *)),
								  this,
								  SLOT(closeProjectFiles(ProjectExplorer::Project *)))
					 : disconnect(m_projectExplorerPlugin->session(),
									  SIGNAL(aboutToRemoveProject(ProjectExplorer::Project *)),
									  this,
									  SLOT(closeProjectFiles(ProjectExplorer::Project *)));

}

bool CloseFilesManager::closeProjectFiles(ProjectExplorer::Project *project)
{
	QStringList filesList(project->files(ProjectExplorer::Project::AllFiles));

	QList<Core::IEditor *> editors;

	for(auto file : filesList)
	{
		editors << Core::ICore::editorManager()->editorsForFileName(file);
	}

	if(!Core::ICore::editorManager()->closeEditors(editors))
	{
		return false;
	}

	return true;
}
