QMAKE_CXXFLAGS += -std=c++11

DEFINES += CLOSEPROJECTFILES_LIBRARY

# CloseProjectFiles files
SOURCES += closeprojectfilesplugin.cpp \
			  settings.cpp \
			  closefilesmanager.cpp

HEADERS += closeprojectfilesplugin.h \
			  closeprojectfiles_global.h \
			  closeprojectfilesconstants.h \
			  settings.h \
			  closefilesmanager.h

# Qt Creator linking
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=C:/Programs/Qt/qt-creator-2.8.1-src

CONFIG(debug, debug|release) {
	IDE_BUILD_TREE = $$(QTC_BUILD)
	isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=C:/Programs/Qt/qt-creator-build/debug
} else {
	IDE_BUILD_TREE = $$(QTC_BUILD)
	isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=C:/Programs/Qt/qt-creator-build/release
}

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

PROVIDER = VPS

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)
include(CloseProjectFiles_dependencies.pri)

LIBS += -L$$IDE_PLUGIN_PATH/QtProject

TRANSLATIONS += closeprojectfiles_ru.ts

RESOURCES += resources.qrc

